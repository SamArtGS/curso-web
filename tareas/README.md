# Tareas de curso de web

Todas las tareas que YO deje se deben entregar en este repositorio siguiendo la metodología de trabajo colaborativo de la comunidad de desarrolladores que utilizan un *controlador d e versiones*, es decir, mediante un **Pull request**.

Para el caso particular de GITLAB los pull request son conocido como *merge request*.

Si tu tarea es un PLAGIO descarado no se aceptará la contribución realizada por medio del *pull request* y se indicará en la sección de comentarios que ha reprobado en la entrega de esa tarea.

Siguiendo la filosofía del programa ustedes deben investigar como hacer el pull request, **no se desesperen si no sale a la primera, nada que valga la pena lo hace**.

Si se topan con conceptos como 

* fork
* branches
* `git remote add --track ...`
* upstream y downstream

Significa que van por el camino correcto :stuck_out_tongue_winking_eye:

Solo para la primera tarea prometo intentar contestar sus dudas en la siguiente dirección de correo rhodfra.proteco@gmail.com, las dudas que tengan debe ser lo más específicas posibles.

`Las tareas son para aprender no para joder gente`

## ¿Dónde entregar su tarea?

Las tareas que les pida las deben entregar en la carpeta que corresponda con el número de tarea que les deje, en este caso si les dejo la tarea 1 la deben entregar siguiendo la siguiente conveción.

```
curso-web/tareas/01/num_prebe-Apellido_paternoNombre
Ejemplos:
curso-web/tareas/01/15-FranciscoRodrigo
curso-web/tareas/01/05-CabreraGalileo
```

Noten que si su número de prebecario es menor que 10 deben poner un cero antes de él, por ejemplo en lugar de solo escribir 5-CabreraGalileo se escribe 05-CabreraGalileo.

*TODO el contenido de su tarea debe estar en la carpeta que ustedes crearon* para el primer ejemplo toda su tarea 1 debe estar en 15-FranciscoRodrigo.

## Listador de tareas

* [Tarea 1](./01/README.md)
* [Tarea 2](./02/README.md)

